import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module'
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { UserService } from './user.service';
import { RegisterService } from './accounts/candidate/register/register.service';
import { RegcompService } from './accounts/company/regcomp/regcomp.service';
import { RegisterComponent } from './accounts/candidate/register/register.component';
import { DashboardsComponent } from './dashboards/dashboards.component';
import { AdminDashComponent } from './dashboards/admin-dash/admin-dash.component';
import { EmployeeDashComponent } from './dashboards/employee-dash/employee-dash.component';
import { CompanyDashComponent } from './dashboards/company-dash/company-dash.component';
import { CandidateDashComponent } from './dashboards/candidate-dash/candidate-dash.component';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './accounts/home/home.component';
import { RegcompComponent } from './accounts/company/regcomp/regcomp.component';
import { AccountsComponent } from './accounts/accounts.component';
import { CandidateComponent } from './accounts/candidate/candidate.component';
import { CompanyComponent } from './accounts/company/company.component';
// import {AuthService} from './auth.service';
// import {TokenInterceptor} from './token.inceptor';

const appRoutes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'candidate', component: CandidateComponent},
  { path: 'company', component: CompanyComponent},   
  { path: 'candidate-register-login', component: RegisterComponent},
  { path: 'company-register-login', component: RegcompComponent},

];

@NgModule({
  declarations: [AppComponent, RegisterComponent, DashboardsComponent, AdminDashComponent, EmployeeDashComponent, CompanyDashComponent, CandidateDashComponent, HomeComponent, RegcompComponent, AccountsComponent, CandidateComponent, CompanyComponent],
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule,RouterModule.forRoot(appRoutes)],
  providers: [UserService,
    RegisterService,
    RegcompService,
    /* AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    } */
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}