import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateDashComponent } from './candidate-dash.component';

describe('CandidateDashComponent', () => {
  let component: CandidateDashComponent;
  let fixture: ComponentFixture<CandidateDashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateDashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateDashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
