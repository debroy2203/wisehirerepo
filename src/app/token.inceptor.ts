import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    // cloning the original request
    request = request.clone({    
      setHeaders: {
        'Content-type': 'application/json',
        Authorization: localStorage.getItem('token')
      }
    });
    return next.handle(request);    // passing control to the next interceptor
  }
}