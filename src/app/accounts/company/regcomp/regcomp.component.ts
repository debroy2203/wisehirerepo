import { Component, OnInit } from '@angular/core';
import {RegcompService} from './regcomp.service'

@Component({
  selector: 'app-regcomp',
  templateUrl: './regcomp.component.html',
  styleUrls: ['./regcomp.component.css']
})
export class RegcompComponent implements OnInit {

  public register: any = {};

  constructor( private _regcompService: RegcompService) { }
  

  ngOnInit() {
    this.register = {
      companyname: '',
      country: '',
      email: '',
      password: ''
    };
  }

  registerCompany(){
    this._regcompService.registerCompany(this.register);
  }

}
