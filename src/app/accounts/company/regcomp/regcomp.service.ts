import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class RegcompService {

  // http options used for making API calls
  private httpOptions: any;


  // error messages received from the login attempt
  public errors: any = [];

  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
  }


// Company Registration
public registerCompany(newUser) {
  return this.http.post<any>('http://127.0.0.1:8000/api/company/', newUser).subscribe(
      data => {
          alert('Company successfully registered');
      },
      err => {
          alert('Not registered.');
          console.error('registration error', err);
          this.errors = err['error'];
      }
  );
} 
}