import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class RegisterService {

  // http options used for making API calls
  private httpOptions: any;


  // error messages received from the login attempt
  public errors: any = [];

  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
  }

  // Candidate Registration
  public registerCandidate(newUser) {
    return this.http.post<any>('http://127.0.0.1:8000/api/candidate/', newUser).subscribe(
        data => {
            alert('Candidate successfully registered');
        },
        err => {
            alert('Not registered.');
            console.error('registration error', err);
            this.errors = err['error'];
        }
    );
} 

}