import { Component, OnInit } from '@angular/core';
import { RegisterService} from './register.service';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public register: any = {};

  constructor(private _registerService: RegisterService) { }
  

  ngOnInit() {
    this.register = {
      fullname: '',
      country: '',
      email: '',
      password: ''
    };
  }

  registerCandidate(){
    this._registerService.registerCandidate(this.register);
  }
 
}
