import {Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { Router } from  '@angular/router';
import {UserService} from './user.service';
import {throwError} from 'rxjs';


 
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  public users: any;
  /**
   * An object representing the user for the login form
   */
  public user: any = {};
  
  public register: any = {};
 
  constructor(private _userService: UserService) { }
 
  ngOnInit() {
    this.user = {
      username: '',
      password: ''
    };
    this.register = {
        username:'',
        email: '',
        password: ''
    };
  }
 
  registerUser(){
    this._userService.registerUser(this.register);
  }

  login() {
    this._userService.login({'username': this.user.username, 'password': this.user.password});
  }
 
  refreshToken() {
    this._userService.refreshToken();
  }
 
  logout() {
    this._userService.logout();
  }
 
  getUsers() {
    this.users = this._userService.getUsers();
  }


}

